﻿using System;
using System.Collections.Generic;
using System.Linq;
using Service.Models;

namespace Service
{
    public class SchedulerService : ISchedulerService
    {
        private int _nrOfShiftsPerDay;

        private int _maxNumberOfShiftsInADayForOnePerson;

        private bool _shiftsInConsecutiveDayAllowed;

        private bool _fairenessRuleEnabled;

        private Random _random;

        public void ConfigureSchedulerService(
            int nrOfShiftsPerDay,
            int maxNumberOfShiftsInADayForOnePerson,
            bool shiftsInConsecutiveDayAllowed,
            bool fairenessRuleEnabled)
        {
            _nrOfShiftsPerDay = nrOfShiftsPerDay;
            _maxNumberOfShiftsInADayForOnePerson = maxNumberOfShiftsInADayForOnePerson;
            _shiftsInConsecutiveDayAllowed = shiftsInConsecutiveDayAllowed;
            _fairenessRuleEnabled = fairenessRuleEnabled;
            _random = new Random();
        }

        /// <summary>
        /// Get the Schedule for the <paramref name="people"/> for <paramref name="numberOfDays"/>/>
        /// </summary>
        /// <param name="people">The people</param>
        /// <param name="numberOfDays">The number of days</param>
        /// <returns>The complete schedule</returns>
        public Schedule GetSchedule(List<Person> people, int numberOfDays)
        {
            var schedule = new Schedule { DayShiftsAllocations = new List<DayShiftsAllocation>() };

            var eligiblePeople = people.ToList();

            for (var i = 1; i <= numberOfDays; i++)
            {
                var dayAllocation = new DayShiftsAllocation { PeopleAllocation = new List<Person>(), DayOrder = i };

                for (var j = 1; j <= _nrOfShiftsPerDay; j++)
                {
                    if (eligiblePeople.Count == 0) //if no eligible people remains 
                    {
                        AddAllEligiblePeople(eligiblePeople, schedule, dayAllocation, i);
                    }

                    if (eligiblePeople.Count == 0)
                    {
                        throw new Exception($"Generation of the schedule for {people.Count} people failed for current configuration");
                    }

                    var pickedPerson = PickRandomPerson(eligiblePeople);

                    dayAllocation.PeopleAllocation.Add(pickedPerson);

                    if (_fairenessRuleEnabled || dayAllocation.PeopleAllocation.Count(x => x.Id == pickedPerson.Id) >= _maxNumberOfShiftsInADayForOnePerson)
                    {
                        eligiblePeople.Remove(pickedPerson);
                    }
                }

                if (_fairenessRuleEnabled || !_shiftsInConsecutiveDayAllowed)
                {
                    dayAllocation.PeopleAllocation.ForEach(x => eligiblePeople.Remove(x));
                }

                schedule.DayShiftsAllocations.Add(dayAllocation);
            }

            return schedule;
        }

        /// <summary>
        /// Add all the eligible people
        /// </summary>
        private void AddAllEligiblePeople(ICollection<Person> eligiblePeople, Schedule schedule, DayShiftsAllocation dayAllocation, int dayNumber)
        {
            if (_shiftsInConsecutiveDayAllowed)
            {
                AddPeopleFromPreviousDays(eligiblePeople, schedule);

                AddPeopleFromTheSameDay(eligiblePeople, dayAllocation);
            }
            else 
            {
                if (dayNumber > 1)
                {
                    AddPeopleFromPreviousDaysExceptingLastOne(eligiblePeople, schedule, dayNumber);
                }

                AddPeopleFromTheSameDay(eligiblePeople, dayAllocation);
            }
        }

        /// <summary>
        /// Add people from previous days excepting the day before
        /// </summary>
        /// <param name="eligiblePeople">The existing eligible people</param>
        /// <param name="schedule">The entire schedule</param>
        private void AddPeopleFromPreviousDays(ICollection<Person> eligiblePeople, Schedule schedule)
        {
            schedule.DayShiftsAllocations.ForEach(x => x.PeopleAllocation.ForEach(y =>
                {
                    if (eligiblePeople.All(q => q.Id != y.Id))
                    {
                        eligiblePeople.Add(y);
                    }
                }
            ));
        }

        /// <summary>
        /// Add people from previous days excepting the last one
        /// </summary>
        /// <param name="eligiblePeople">The existing eligible people</param>
        /// <param name="schedule">The entire schedule</param>
        /// <param name="dayNumber">The day numnber</param>
        private void AddPeopleFromPreviousDaysExceptingLastOne(ICollection<Person> eligiblePeople, Schedule schedule, int dayNumber)
        {
            schedule.DayShiftsAllocations.ForEach(x =>
                x.PeopleAllocation.ForEach(y =>
                {
                    if (schedule.DayShiftsAllocations[dayNumber - 2].PeopleAllocation.All(z => z.Id != y.Id) &&
                        eligiblePeople.All(q => q.Id != y.Id))
                    {
                        eligiblePeople.Add(y);
                    }
                }));
        }

        /// <summary>
        /// Add the people from the same day who have not reached their maximum limit per day
        /// </summary>
        /// <param name="eligiblePeople">The existing eligible people</param>
        /// <param name="dayAllocation">The allocation of the day</param>
        private void AddPeopleFromTheSameDay(ICollection<Person> eligiblePeople, DayShiftsAllocation dayAllocation)
        {
            dayAllocation.PeopleAllocation
                .Where(x => dayAllocation.PeopleAllocation.Count(y => y == x) < _maxNumberOfShiftsInADayForOnePerson).ToList()
                .ForEach(x =>
                {
                    if (eligiblePeople.All(q => q.Id != x.Id))
                    {
                        eligiblePeople.Add(x);
                    }
                });
        }

        /// <summary>
        /// Pick a random person from the <paramref name="people"/>/>
        /// </summary>
        private Person PickRandomPerson(IReadOnlyList<Person> people)
        {
            return people[_random.Next(0, people.Count)];
        }
    }
}
