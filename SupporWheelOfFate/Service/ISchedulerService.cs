﻿using Service.Models;
using System.Collections.Generic;

namespace Service
{
    public interface ISchedulerService
    {
        Schedule GetSchedule(List<Person> people, int numberOfDays);

        void ConfigureSchedulerService(int nrOfShiftsPerDay, int maxNumberOfShiftsInADayForOnePerson,
            bool shiftsInConsecutiveDayAllowed, bool fairenessRuleEnabled);
    }
}
