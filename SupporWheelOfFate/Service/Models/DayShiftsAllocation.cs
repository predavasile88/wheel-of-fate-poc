﻿using System.Collections.Generic;

namespace Service.Models
{
    public class DayShiftsAllocation
    {
        public List<Person> PeopleAllocation { get; set; }

        public int DayOrder { get; set; }
    }
}
