﻿using System.Collections.Generic;

namespace Service.Models
{
    public class Schedule
    {
        public List<DayShiftsAllocation> DayShiftsAllocations { get; set; }
    }
}
