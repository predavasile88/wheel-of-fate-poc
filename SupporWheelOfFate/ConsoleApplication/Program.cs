﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Service;
using Service.Models;
using System.Web.Script.Serialization;

namespace ConsoleApplication
{

    //Console Application Used for Development and Testing purposes
    class Program
    {
        static void Main(string[] args)
        {
            var schedulerService = GetDefaultSchedulerService();
            //========================Case 1======================
            //schedulerService.ConfigureSchedulerService(
            //    nrOfShiftsPerDay: 4,
            //    maxNumberOfShiftsInADayForOnePerson: 1,
            //    shiftsInConsecutiveDayAllowed: false,
            //    fairenessRuleEnabled: false);

            //========================Case 2======================
            schedulerService.ConfigureSchedulerService(
                nrOfShiftsPerDay: 8,
                maxNumberOfShiftsInADayForOnePerson: 1,
                shiftsInConsecutiveDayAllowed: true,
                fairenessRuleEnabled: true);

            var schedule = new Schedule();

            try
            {
                schedule = schedulerService.GetSchedule(people: GetMockupListOfPeople(10), numberOfDays: 10);
                PrintSchedule(schedule);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        private static List<Person> GetMockupListOfPeople(int numberOfPeople)
        {
            var result = new List<Person>();

            for (var i = 0; i < numberOfPeople; i++)
            {
                result.Add(new Person { Id = i, Name = $"Person {i}" });

            }

            var resultJson = new JavaScriptSerializer().Serialize(result);

            return result;
        }

        private static void PrintSchedule(Schedule schedule)
        {
            for (var i = 0; i < schedule.DayShiftsAllocations.Count; i++)
            {
                Console.WriteLine($"Day {i + 1}:", ConsoleColor.Blue);
                foreach (var person in schedule.DayShiftsAllocations[i].PeopleAllocation)
                {
                    Console.Write($"{person.Name}; ");
                }
                Console.WriteLine();
            }
        }

        private static SchedulerService GetDefaultSchedulerService()
        {
            var nrOfShiftsPerDay = Convert.ToByte(ConfigurationManager.AppSettings["nrOfShiftsPerDay"]);
            var maxNumberOfShiftsInADayForOnePerson = Convert.ToByte(ConfigurationManager.AppSettings["maxNumberOfShiftsInADayForOnePerson"]);
            var shiftsInConsecutiveDayAllowed = Convert.ToBoolean(ConfigurationManager.AppSettings["shiftsInConsecutiveDayAllowed"]);
            var fairenessRuleEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["fairenessRuleEnabled"]);

            var schedulerService = new SchedulerService();
            schedulerService.ConfigureSchedulerService(nrOfShiftsPerDay, maxNumberOfShiftsInADayForOnePerson, shiftsInConsecutiveDayAllowed, fairenessRuleEnabled);

            return schedulerService;
        }
    }
}
