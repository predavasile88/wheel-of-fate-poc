﻿using AutoMapper;
using Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WheelOfFateApi.Utils
{
    public class WebMapper : Profile
    {
        public WebMapper()
        {
            CreateMap<Person, Service.Models.Person>().ReverseMap();
            CreateMap<Schedule, Service.Models.Schedule>().ReverseMap();
            CreateMap<DayShiftsAllocation, Service.Models.DayShiftsAllocation>().ReverseMap();
        }
    }
}