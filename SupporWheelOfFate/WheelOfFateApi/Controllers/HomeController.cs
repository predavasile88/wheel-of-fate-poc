﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Service;
using WheelOfFateWeb.Models;

namespace WheelOfFateWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISchedulerService _schedulerService;

        public HomeController(ISchedulerService schedulerService)
        {
            _schedulerService = schedulerService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            WheelOfFateModel model = WheelOfFateModel.GetDefaultModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(WheelOfFateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _schedulerService.ConfigureSchedulerService(
                       model.NrOfShiftsPerDay.Value,
                       model.MaxNumberOfShiftsInADayForOnePerson.Value,
                       model.ShiftsInConsecutiveDayAllowed,
                       model.FairenessRuleEnabled);

                    var peopleList = model.People.Select(Mapper.Map<Person, Service.Models.Person>).ToList();

                    var result = Mapper.Map<Service.Models.Schedule, Schedule>(_schedulerService.GetSchedule(peopleList, model.NrOfDaysForSchedule.Value));

                    model.Allocations = result.DayShiftsAllocations;
                }
                catch (Exception ex)
                {
                    model.Error = ex.Message;
                }
            }

            return View(model);
        }

        public PartialViewResult GetNewPerson(int id)
        {
            Person model = new Person()
            {
                Id = id,
                Name = $"Person{id}"
            };
            return PartialView("Person", model);
        }
    }
}