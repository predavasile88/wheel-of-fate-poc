﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using Service;
using WheelOfFateApi.App_Start;
using WheelOfFateWeb.Models;

namespace WheelOfFateWeb.Controllers
{
    public class ServiceController : ApiController
    {
        private readonly ISchedulerService _schedulerService;

        public ServiceController(ISchedulerService schedulerService)
        {
            _schedulerService = schedulerService;
        }

        [HttpPost]
        [Route("service/createSchedule")]
        public Schedule CreateSchedule(
            [FromBody]List<Person> people,
            [SwaggerDefaultValue("10")]
            int nrOfDaysForSchedule = 10,
            int nrOfShiftsPerDay = 2,
            int maxNumberOfShiftsInADayForOnePerson = 1,
            bool shiftsInConsecutiveDayAllowed = false,
            bool fairenessRuleEnabled = false)
        {

            _schedulerService.ConfigureSchedulerService(
                nrOfShiftsPerDay, maxNumberOfShiftsInADayForOnePerson,
                shiftsInConsecutiveDayAllowed, fairenessRuleEnabled);

            var peopleList = people.Select(Mapper.Map<Person, Service.Models.Person>).ToList();

            return Mapper.Map<Service.Models.Schedule, Schedule>(_schedulerService.GetSchedule(peopleList, nrOfDaysForSchedule));
        }
    }
}