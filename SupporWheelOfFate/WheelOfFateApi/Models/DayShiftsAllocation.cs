﻿using System.Collections.Generic;

namespace WheelOfFateWeb.Models
{
    public class DayShiftsAllocation
    {
        public int DayOrder { get; set; }

        public List<Person> PeopleAllocation { get; set; }
    }
}
