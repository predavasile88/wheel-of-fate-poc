﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WheelOfFateWeb.Models
{
    public class WheelOfFateModel
    {
        public WheelOfFateModel()
        {
            People = new List<Person>();
            Allocations = new List<DayShiftsAllocation>();
        }

        [Required]
        [Range(5, 50)]
        [Display(Name = "Nr of days")]
        public int? NrOfDaysForSchedule { get; set; }

        [Required]
        [Range(2, 8)]
        [Display(Name = "Shifts per day")]
        public int? NrOfShiftsPerDay { get; set; }

        [Required]
        [Range(1, 8)]
        [Display(Name = "Max shifts per person")]
        public int? MaxNumberOfShiftsInADayForOnePerson { get; set; }
        
        [Display(Name = "Shifts in consecutive days")]
        public bool ShiftsInConsecutiveDayAllowed { get; set; }

        [Display(Name = "Fairness")]
        public bool FairenessRuleEnabled { get; set; }

        public List<Person> People { get; set; }

        public List<DayShiftsAllocation> Allocations { get; set; }

        public string Error { get; set; }

        public static WheelOfFateModel GetDefaultModel()
        {
            WheelOfFateModel result = new WheelOfFateModel()
            {
                NrOfDaysForSchedule = 10,
                NrOfShiftsPerDay = 2,
                MaxNumberOfShiftsInADayForOnePerson = 1,
                FairenessRuleEnabled = true
            };

            for (int i = 0; i < 10; i++)
            {
                result.People.Add(new Person() { Id = i + 1, Name = $"Person{i + 1}" });
            }
            return result;
        }
    }
}