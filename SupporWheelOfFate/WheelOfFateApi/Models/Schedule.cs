﻿using System.Collections.Generic;

namespace WheelOfFateWeb.Models
{
    public class Schedule
    {
        public List<DayShiftsAllocation> DayShiftsAllocations { get; set; }
    }
}
