﻿using System;

namespace WheelOfFateWeb
{
    public class SwaggerDefaultValue : Attribute
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public Type Type { get; set; }

        public SwaggerDefaultValue(string value)
        {
            this.Value = value;
            this.Type = typeof(string);
        }
    }
}